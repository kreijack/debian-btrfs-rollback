
prefix = /usr/

# NOTE: start the subvolume name with '/'. E.g. '/boot' is valid, 'boot' is NOT valid
svrootfs=/@rootfs
svrollback=/@rollback
svinvalid=/@rootfs_invalid

all:

install:
	-mkdir -p $(DESTDIR)/etc/default
	-mkdir -p $(DESTDIR)/etc/default/grub.d
	-mkdir -p $(DESTDIR)$(prefix)/bin
	-mkdir -p $(DESTDIR)/etc/initramfs-tools/scripts
	-mkdir -p $(DESTDIR)/etc/apt/apt.conf.d
	sed -e "s @SVROOTFS@ $(svrootfs) g" \
		-e "s @SVROLLBACK@ $(svrollback) g" \
		<initramfs/btrfs.in \
		>$(DESTDIR)/etc/initramfs-tools/scripts/btrfs
	sed -e "s @SVROOTFS@ $(svrootfs) g" \
		-e "s @SVROLLBACK@ $(svrollback) g" \
		<conf/debian-btrfs-rollback.in \
		>$(DESTDIR)/etc/default/debian-btrfs-rollback
	sed -e "s @SVROOTFS@ $(svrootfs) g" \
		-e "s @SVROLLBACK@ $(svrollback) g" \
		<grub/debian-btrfs-rollback.cfg.in \
		>$(DESTDIR)/etc/default/grub.d/debian-btrfs-rollback.cfg
	cp bin/debian-btrfs-rollback.sh $(DESTDIR)$(prefix)/bin
	cp apt.conf.d/10btrfs.conf $(DESTDIR)/etc/apt/apt.conf.d
	cp profile/debian-btrfs-rollback.sh $(DESTDIR)/etc/profile.d

clean:

distclean: clean

uninstall:
	-rm -f $(DESTDIR)$(prefix)/bin/debian-btrfs-rollback
	-rm -f $(DESTDIR)/etc/default/debian-btrfs-rollback
	-rm -f $(DESTDIR)/etc/initramfs-tools/scripts/btrfs

.PHONY: all install clean distclean uninstall

