#!/bin/bash

. /etc/default/debian-btrfs-rollback

btrfsroot="$SVROOTFS"
btrfsrollback="$SVROLLBACK"
btrfsbroken="$BROKENSVROOTFS"
#logfile=/var/log/debian-btrfs-rollback.log
if [ -z "$logfile" ]; then
	logfile=/dev/null
fi


# $ cat /proc/self/mountinfo 
# 21 27 0:19 / /sys rw,nosuid,nodev,noexec,relatime shared:7 - sysfs sysfs rw
# 27 1 0:23 /debian / rw,noatime,nodiratime shared:1 - btrfs /dev/sde3 rw,nossd,space_cache,subvolid=257,subvol=/debian
# ....

# prerequisite:
# - tail
# - awk
# - sed
# - bash
# - btrfs
# - stat

# the following functions get the properties of the current 'root' subvolume

get_root_subvol() {
	cat /proc/self/mountinfo | awk '{ if ($5 == "/") print $4 }' | tail -1
}

get_root_dev() {
	cat /proc/self/mountinfo | awk '{ if ($5 == "/") print $10 }' | tail -1
}

get_root_options_ext() {
	cat /proc/self/mountinfo | awk '{ if ($5 == "/") print $11 }' | tail -1
}

get_root_options() {
	cat /proc/self/mountinfo | awk '{ if ($5 == "/") print $6 }' | tail -1
}

get_root_fstype() {
	cat /proc/self/mountinfo | awk '{ if ($5 == "/") print $9 }' | tail -1
}

# check if the current root subvolume is the one admitted
check_root_subvolume() {
	cursv="$(get_root_subvol)"
	[ "x$cursv" = "x$btrfsroot" ] && return 0
	[ "x$cursv" = "x$btrfsrollback" ] && return 0
	
	return 1
}

check_root_filesystem() {
	curfstype="$(get_root_fstype)"
	if [ "x$curfstype" != "xbtrfs" ]; then
		return 1
	else
		return 0
	fi
}

check_is_subvol() {
	fsn="$(stat -f -c "%T" "$1")"
	[ "x$fns" != "xbtrfs" ] && return 1

	fsi="$(stat -c "%i" "$1")"
	[ "x$i" != "256" ] && return 1

	return 0
}

wait_dir() {
	i=20
	while [ $i -gt 0 -a -d "$1" ]; do
		i=$(( $i + 1 ))
		sleep 0.1
	done

	if [ "x$i" = "x0" ]; then
		return 1
	else
		return 0
	fi
}

err_msg() {
	echo 1>&2 "****"
	echo 1>&2 "**** $1"
	echo 1>&2 "****"
}

drop_subvolume_or_exit() {
	tmpdir="$1"
	svname="$2"
	if [ -d "$tmpdir/$svname" ]; then
		if ! (btrfs subvolume delete "$tmpdir/$svname" &&
		     wait_dir "$tmpdir/$svname") ; then
			err_msg "Cannot delete '$svname' subvolume. ABORT"
			exit 1
		fi
	fi
}

make_rollback_snapshot() {
	check_root_filesystem || return 1
	rdev="$(get_root_dev)"

	tmpdir="$(mktemp --directory)"

	rm -f /run/.debian-btrfs-rollback.rollback
	rm -f /run/.debian-btrfs-rollback.broken

	mount -o "subvol=/" "$rdev" "$tmpdir" && (
		drop_subvolume_or_exit "$tmpdir" "$btrfsrollback" &>$logfile
		drop_subvolume_or_exit "$tmpdir" "$btrfsbroken" &>$logfile

		btrfs subvolume snapshot / "$tmpdir/$btrfsrollback" || (
			err_msg "Cannot perform the rootfs snapshot"
			exit 100
		)
	) && (
		echo "Snapshot filesystem"
	)
	
	err=$?
	umount "$tmpdir"
	rmdir "$tmpdir"

	return "$err"
}

boot_cleanup() {
	check_root_filesystem || return 1

	if ! check_root_subvolume; then
		err_msg "For '/' a foreign subvolume is used. ABORT"
		exit 1
	fi

	rdev="$(get_root_dev)"
	tmpdir="$(mktemp --directory)"

	mount -o "subvol=/" "$rdev" "$tmpdir" && (
		drop_subvolume_or_exit "$tmpdir" "$btrfsbroken" &>$logfile

		rsv="$(get_root_subvol)"
		set -e
		#
		# if the root subvolume is the default root one, do nothing
		#
		#if [ "x$rsv" = "x$btrfsroot" ]; then
		#	if [ -d "$tmpdir/$btrfsrollback" ]; then
		#		mv "$tmpdir/$btrfsrollback" "$tmpdir/$btrfsbroken"
		#	fi
		#fi
		if [  "x$rsv" = "x$btrfsrollback" ]; then
			if [ -d "$tmpdir/$btrfsroot" ]; then
				mv "$tmpdir/$btrfsroot" "$tmpdir/$btrfsbroken"
			fi
			mv "$tmpdir/$btrfsrollback" "$tmpdir/$btrfsroot"
		fi

		if [ -d "$tmpdir/$btrfsrollback" ]; then
			touch /run/.debian-btrfs-rollback.rollback
		fi
		if [ -d "$tmpdir/$btrfsbroken" ]; then
			touch /run/.debian-btrfs-rollback.broken
		fi
	) && (
                echo "Boot cleanup filesystem"
        ) || (
		err_msg "Cannot swap subvolumes"
		exit 100
	)
	err="$?"

	umount "$tmpdir"
	rmdir "$tmpdir"

	return "$err"
}

drop_unneeded_subvolumes() {
	check_root_filesystem || return 1
	cursv="$(get_root_subvol)"
	if [ "x$cursv" != "x$btrfsroot" ]; then
		err_msg "For '/' an invalid subvolume is used. ABORT"
		exit 1
	fi

	rdev="$(get_root_dev)"
	tmpdir="$(mktemp --directory)"

	mount -o "subvol=/" "$rdev" "$tmpdir" && (
		drop_subvolume_or_exit "$tmpdir" "$btrfsrollback" &>$logfile
		drop_subvolume_or_exit "$tmpdir" "$btrfsbroken" &>$logfile
	) && (
                echo "Cleanup filesystem"
        )

	err="$?"

	umount "$tmpdir"
	rmdir "$tmpdir"

	return "$err"

}

check_subvols() {
	if [ -e /run/.debian-btrfs-rollback.rollback ]; then
		echo "**** WARNING: The rollback subvolume exists: the last"
		echo "**** WARNING: update was interrupted or is ongoing."
		echo "**** WARNING: Usually no action has to taken."
	fi	
	if [ -e /run/.debian-btrfs-rollback.broken ]; then
		echo "**** WARNING: The broken subvolume exists: the last"
		echo "**** WARNING: update was interrupted, and the reboot"
		echo "**** WARNING: resumed the snapshot subvolume taken"
		echo "**** WARNING: before."
		echo "**** WARNING: It is suggested to check the subvolumes or"
		echo "**** WARNING: to restart the update process."
	fi
}

main() {
	if ! check_root_filesystem; then
		err_msg "For '/' a NON BTRFS filesystem is used. ABORT"
		exit 1
	fi
	if [ "$btrfsrollback" = "${btrfsrollback#/}" ]; then
		err_msg "SVROLLBACK must start with '/'"
		exit 99
	fi
	if [ "$btrfsroot" = "${btrfsroot#/}" ]; then
		err_msg "SVROOOT must start with '/'"
		exit 99
	fi
	if [ "$btrfsbroken" = "${btrfsbroken#/}" ]; then
		err_msg "BROKENSVROOOT must start with '/'"
		exit 99
	fi


	if [ "$1" = "snapshot" ]; then
		make_rollback_snapshot
	elif [ "$1" = "clean" ]; then
		drop_unneeded_subvolumes
	elif [ "$1" = "boot-clean" ]; then
		if [ "x$ENABLEATBOOT" = "xyes" ]; then
			boot_cleanup
			track_subvols
		else
			echo "This function is disabled by config file"
		fi
	elif [ "$1" = "drop-subvolumes" ]; then
		drop_unneeded_subvolumes
	elif [ "$1" = "check" ]; then
		check_subvols
	else
		echo "usage: $0 [--debug] snapshot|clean|boot-clean|drop-subvolumes|check"
	fi
}

if [ "x$1" = "x--debug" ]; then
	shift
	logfile=/dev/tty
	set -x 
fi

main "$@"

