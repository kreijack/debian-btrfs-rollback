# DEBIAN-BTRFS-ROLLBACK

Note: this package is debian specific, and it makes sense only if btrfs is
used as root filesystem.

## INTRODUCTION

This package provides a set of configuration files to allow the system to
rollback to the previous state in case of a sudden shutdown during
an updated by apt.

The basic idea is to take a snapshot of the system before the upgrade, and
then restore it in case of a sudden shutdown. Instead if everything ends
sucessful, this snapshot is destroyed.

The restore of the system is done at boot time (at initramfs level): if
the rollback subvolume exists, it is mounted at boot time instead the
default one.

The real goal is to avoid the performance penalty that DPKG suffer when
the root filesystem is BTRFS. Creating a snapshot before the upgrade
procedure, allow to disable all the sync()/fsync() calls that DPKG
does to preserve the filesystem coherence.

## FILESYSTEM LAYOUT

To work properly, this package needs that the root filesystem is in a 
BTRFS subvolume. The following names are used for the subvolumes, even tough 
these can be changed modifying the Makefile at installation time or updating
the configuration file by hand.

| Description       | Subvolume name   | Note                                |
|-------------------|------------------|-------------------------------------|
|Root filesystem    | @rootfs          |real root filesystem                 |
|Rollback subvolume | @rollback        |snapshot taken before the update     |
|Broken subvolume   | @rootfs.broken   |old root subvolume                   |

A subvolume name starts with an '@' as first character. This is not
needed, but is a convention widely used.

Because the snapshot ends to subvolume boundary, it is suggested that some
directories of the filesystem live in separate subvolumes and are mounted at
boot time via /etc/fstab. The table below suggests some subvolumes:

| Subvolume name   | Mount point         |
|------------------|---------------------|
| @rootfs          | /                   |
| @boot            | /boot               |
| @log             | /var/log            |
| @portables       | /var/lib/portables  |
| @home            | /home               |


Other sub directories candidates to live in an own subvolume are the ones used
by database, virtual machine and/or other application outside of the system
upgrade procedure.

## UPGRADE PROCEDURE

As first step of an ```apt upgrade``` (or to be more precise after the download
and before the real packages update) a snapshot of the ```@rootfs``` subvolume
is taken and it is called ```@rollback```.
When the upgrade ends, the ```@rollback``` subvolume is deleted. 
If the ```@rollback``` subvolume exists at boot time, this means that the
upgrade process was interrupted by a sudden shutdown.

Before the snapshot, the subvolume ```@rollback``` and ```@rootfs.broken``` are
deleted (if any exists).

## BOOT PROCEDURE

There are two main step:

1. **initramfs**

The initramfs tries to mount the ```@rollback``` subvolume as root filesystem,
if this fails, the ```@rootfs``` subvolume is mounted as root fs.

2. **systemd unit**

During the boot, if the root filesystem uses ```@rollback``` subvolume the
following action are performed:

   1. rename ```@rootfs``` subvolume to ```@rootfs.broken```
   2. rename ```@rollback``` subvolume to ```@rootfs```

This means that the user still have to possibility to check the old root
filesystem looking at the ```@rootfs.broken``` subvolume.
